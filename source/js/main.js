// Для добавления функционала используем классы с префиксом js, стилизовать по этим классам нельзя

$(document).ready(function(){

	// $('input[type=tel]')
	// 	.inputmask("8 (999) 999 99 99");


	// $('.js-popup-img')
	// 	.magnificPopup({
	// 		type:'image',
	// 		closeOnContentClick: true,
	// 		fixedContentPos: true,
	// 		mainClass: 'mfp-no-margins mfp-with-zoom',
	// 		image: {
	// 			verticalFit: true
	// 		},
	// 		zoom: {
	// 			enabled: true,
	// 			duration: 300
	// 		}
	// 	});

    $('.js-openMenu').click(function(){
        $(this).toggleClass('openMenu_active');
        $('.js-body').toggleClass('body_overflow');
        $('.js-header').toggleClass('header_openMenu');
        $('.js-topMenu').toggleClass('topMenu_show');
        $('.js-socials').toggleClass('socials_show');
    })
});
